package com.example.user.myloginapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Button mMapBtn = (Button)findViewById(R.id.map_button);
        mMapBtn.setOnClickListener(this);

        Button mAccBtn = (Button)findViewById(R.id.acc_button);
        mAccBtn.setOnClickListener(this);

        Button mSpeedDataBtn = (Button)findViewById(R.id.speeddata_button);
        mSpeedDataBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v){

        Intent intent;


        if(v.getId() == R.id.map_button){
            intent = new Intent(HomeActivity.this,MyMapViewActivity.class);
            startActivity(intent);
        }
        else if(v.getId() == R.id.acc_button){
            intent = new Intent(HomeActivity.this,SensorActivity.class);
            startActivity(intent);
        }
        else if(v.getId() == R.id.speeddata_button){
            Toast.makeText(HomeActivity.this,"Speed Data View, To be supported!!!", Toast.LENGTH_LONG).show();
        }
    }
}
