package com.example.user.myloginapp;

import android.*;
import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;

public class MyMapViewActivity extends FragmentActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private GoogleMap mMap;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    LatLng mLatLng;
    Marker mCurrLocationMarker;
    private Location mPrevLocation;


    private ArrayList<LatLng> mLocPoints;
    Polyline mPath;
    float mSpeed = (float)0.0;
    float mZoomLevel =(float)15.0;//zoom value for map to be set between 2 and 21. 15 seemed appropriate

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_map_view);

        mLocPoints = new ArrayList<LatLng>();

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            Toast.makeText(this,"Location Services not available!!!",Toast.LENGTH_SHORT).show();
            return;
        }
        mMap.setMyLocationEnabled(true);
        buildGoogleApiClient();

        //implemented long press for testing purpose
        /*mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                mMap.addMarker(new MarkerOptions().position(latLng).title(
                        latLng.toString()));

                mLocPoints.add(latLng);
                redrawPathLine();
            }
        });*/
    }


    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mGoogleApiClient.connect();
    }

    @Override
    protected void onResume() {
        super.onResume();
        startLocationRequest();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopLocationRequest();
    }


    @Override
    public void onConnected(Bundle bundle) {

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            Toast.makeText(this,"Location Services not available!!!",Toast.LENGTH_SHORT).show();
            return;
        }

        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);

        if (mLastLocation != null) {
            //place marker at current position
            mLatLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(mLatLng);
            markerOptions.title(getString(R.string.curr_pos));
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
            mCurrLocationMarker = mMap.addMarker(markerOptions);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mLatLng,mZoomLevel));

            mLocPoints.add(mLatLng);
            redrawPathLine();
        }

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000); //5 seconds
        mLocationRequest.setFastestInterval(3000); //3 seconds
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        startLocationRequest();
    }

    private void startLocationRequest() {
        if(mGoogleApiClient != null) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            }
        }
    }

    private void stopLocationRequest() {
        if(mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Toast.makeText(this,"onConnectionSuspended",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Toast.makeText(this,"onConnectionFailed",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLocationChanged(Location location) {//incoming agrument location is current location

        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        mLatLng = new LatLng(location.getLatitude(), location.getLongitude());
        //calculate speed using current location
        calculateSpeed(location);

        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(mLatLng);
        markerOptions.title(getString(R.string.curr_pos));
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
        mCurrLocationMarker = mMap.addMarker(markerOptions);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mLatLng,mZoomLevel));

        mLocPoints.add(mLatLng);
        redrawPathLine();
    }

    private void calculateSpeed(Location currLoc) {
        float speed = (float)0.0;
        float distance;

        if(this.mPrevLocation != null){

            //calculate distance
            distance = calculatedistance(currLoc);

            //converting time difference from milliseconds to seconds
            speed = distance /((currLoc.getTime() - mPrevLocation.getTime())/1000);
        }
        mSpeed = (float)speed;
        Toast.makeText(this,"mSpeed : "+mSpeed, Toast.LENGTH_LONG).show();

        mPrevLocation = currLoc;
    }

    //Distance calculation using Haversine formula
    private float calculatedistance(Location currLocation) {
        double currLat = currLocation.getLatitude();
        double currLong = currLocation.getLongitude();
        double prevLat = mPrevLocation.getLatitude();
        double prevLong = mPrevLocation.getLongitude();

        double earthRadius = 6371000.0; //in meters

        double diffInLat = Math.toRadians(currLat - prevLat);
        double diffInLong = Math.toRadians(currLong - prevLong);

        double val = Math.sin(diffInLat/2) * Math.sin(diffInLat/2) +
                Math.cos(Math.toRadians(prevLat)) * Math.cos(Math.toRadians(currLat)) *
                        Math.sin(diffInLong/2) * Math.sin(diffInLong/2);

        double tanVal = 2 * Math.atan2(Math.sqrt(val), Math.sqrt(1-val));
        float dist = (float)(earthRadius * tanVal);

        //Toast.makeText(this,"distance: "+ dist, Toast.LENGTH_LONG).show();
        return dist;
    }

    private void redrawPathLine() {
        float speedInKM = (float)0.0;

        PolylineOptions options =  new PolylineOptions().width(5).color(Color.GREEN).geodesic(true);

        //path line in different colours based on speed.
        //GREEN less than 20km speed
        //YELLOW between 20km and 40km
        //RED greater than 40 km

        //To convert Mts/sec to KM/hr, the conversion factor is 1 m/s = (18/5) KM/hr
        speedInKM = mSpeed * ((float)(18/5)) ;

        Toast.makeText(this,"speed in KM : "+speedInKM, Toast.LENGTH_LONG).show();

        //Smaller distance can be used to test the app by walk
        /*if(speedInKM <= ((float) 2.0)){
            options.color(Color.GREEN);
        }
        else if(speedInKM > ((float)2.0) && speedInKM <= ((float)4.0)){
            options.color(Color.YELLOW);
        }
        else {//more than 4km
            options.color(Color.RED);
        }*/

        if(speedInKM <= ((float) 20.0)){
            options.color(Color.GREEN);
        }
        else if(speedInKM > ((float)20.0) && speedInKM <= ((float)40.0)){
            options.color(Color.YELLOW);
        }
        else {//more than 40km
            options.color(Color.RED);
        }

        for (int i = 0; i < mLocPoints.size(); i++) {
            LatLng point = mLocPoints.get(i);
            options.add(point);
        }
        mPath = mMap.addPolyline(options); //draw line between points
    }
}
